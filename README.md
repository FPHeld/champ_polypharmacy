##Git Repository for the source code to "Association rule analysis   and medication complexity: potential implications in monitoring safety of medication exposures in older adults"##

#Summary#

This project uses frequent set analysis and association rules to identify patterns of polypharmacy in a cross-sectional data set from the CONCORD HEALTH AND AGEING IN MEN PROJECT at the University of Sydney and the Concord Hospital, Sydney, Australia.

#What is this repository for?#

This repository makes available the source code of the underlying analysis. It is written in R, importing especially libraries "arules" and "ggplot2".

How do I get set up?

This repo is still under development. It contains several different R scripts.

* AnalyseArules_3.R: This script conducts the analysis
* Auxiliary2.R: defining functions about the layout and visualisation of arules results



To replicate the arules analysis, you will only need the auxiliary functions and the AnalyseArules_3.R script. The input format to this script needs to be a binary matrix as csv file with individual diseases in columns and cases in rows.
Who do I talk to?

    Repo owner is Fabian Held at the University of Sydney.

The code is not yet written for straight forward application to other data sets. There may be bugs and incompatibilities. Please contact me if you are interested in using or adapting the code and run into trouble.