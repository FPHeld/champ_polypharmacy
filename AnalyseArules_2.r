library("arules")
library("colorspace")
library("reshape2")
library("dplyr")
library("ggplot2")
library("lazyeval")
library("readxl")


source("Auxiliary.R")


data <- as.data.frame(read_excel(path=".\\rawdata\\champ_baseline_anygs_and_drugs July7.xlsx", sheet = 1 ))
data <- data.frame( sapply(data, as.numeric))

str(data)
############ Eclat analysis for most frequent patterns overall ########
data %>% filter(ANYGS==1) %>% select(-champid, -ANYGS) %>% as.matrix() %>% as("transactions") -> data_transactions_ANYGS
data %>% filter(ANYGS==0) %>% select(-champid, -ANYGS) %>% as.matrix() %>% as("transactions") -> data_transactions_NOGS


#ignoring syndromes - only looking at prescriptions
most_frequent_patterns_ANYGS <-  eclat(data_transactions_ANYGS, 
                                         parameter=new("ECparameter", 
                                                       tidLists=TRUE, 
                                                       support = 0.05, 
                                                       ext = TRUE, 
                                                       minlen=2, 
                                                       maxlen=ncol(data_transactions)
                                                       ), 
                                         control=  new("ECcontrol", verbose = FALSE)
                                         )
most_frequent_patterns_NOGS <-  eclat(data_transactions_NOGS, 
                                         parameter=new("ECparameter", 
                                                       tidLists=TRUE, 
                                                       support = 0.05, 
                                                       ext = TRUE, 
                                                       minlen=2, 
                                                       maxlen=ncol(data_transactions)
                                                       ), 
                                         control=  new("ECcontrol", verbose = FALSE)
                                         )
length(most_frequent_patterns_ANYGS)
inspect(most_frequent_patterns_ANYGS)
length(most_frequent_patterns_NOGS)
inspect(most_frequent_patterns_NOGS)

matrix_N_Cases_ANYGS <- select_and_multiply(conditional_frequent_sets = most_frequent_patterns_ANYGS, 
                                      TF_interestingness = rep(TRUE, length(most_frequent_patterns_ANYGS)),
                                      values = quality(most_frequent_patterns_ANYGS)$transIdenticalToItemsets)  # extract the counts
 

matrix_N_Cases_NOGS <- select_and_multiply(conditional_frequent_sets = most_frequent_patterns_NOGS, 
                                      TF_interestingness = rep(TRUE, length(most_frequent_patterns_NOGS)),
                                      values = quality(most_frequent_patterns_NOGS)$transIdenticalToItemsets)  # extract the counts
 

greens <- sequential_hcl(2, h = 140, c = c(0, 50), l = c(100, 25), power=1) 



####### Create Heatmap for most frequent patterns overall ############
longData_ANYGS <- melt(matrix_N_Cases_ANYGS)
#head(longData, 20)
 

longData_ANYGS$Var2 <- factor(longData_ANYGS$Var2)
longData_ANYGS$Var1 <- factor(longData_ANYGS$Var1)
longData_ANYGS %>% group_by(Var1) %>% summarise(RowMaxCases= max(value)) %>% arrange(RowMaxCases) -> Maxes_ANYGS
Maxes_ANYGS$Y <- 1:nrow(Maxes_ANYGS)

longData_ANYGS <- full_join(longData_ANYGS, Maxes_ANYGS)



hm_Overall_ANYGS <- make_one_heatmap.gg(longData_ANYGS, xVar="Var2", yVar="Y", fillVar="value",
                        my_colors=greens, 
                        overall_max=150, 
                        this_main="Most Frequent Patterns of Prescription Combinations with Geriatric Syndromes", 
                        this_title="Frequency")
hm_Overall_ANYGS

####### Create Heatmap for most frequent patterns overall ############
longData_NOGS <- melt(matrix_N_Cases_NOGS)
#head(longData, 20)
 

longData_NOGS$Var2 <- factor(longData_NOGS$Var2)
longData_NOGS$Var1 <- factor(longData_NOGS$Var1)
longData_NOGS %>% group_by(Var1) %>% summarise(RowMaxCases= max(value)) %>% arrange(RowMaxCases) -> Maxes_NOGS
Maxes_NOGS$Y <- 1:nrow(Maxes_NOGS)

longData_NOGS <- full_join(longData_NOGS, Maxes_NOGS)



hm_Overall_NOGS <- make_one_heatmap.gg(longData_NOGS, xVar="Var2", yVar="Y", fillVar="value",
                        my_colors=greens, 
                        overall_max=150, 
                        this_main="Most Frequent Patterns of Prescription Combinations without Geriatric Syndromes", 
                        this_title="Frequency")
hm_Overall_NOGS

dir.create(".\\Output\\")
pdf(file=paste0(".\\Output\\", format(Sys.time(), "%Y_%m_%d"), "_Heatmap_of_Most_Frequent_Patterns_w.wo_Syndromes.pdf"),
    width= 7.5, paper="special")
plot(hm_Overall_ANYGS)
plot(hm_Overall_NOGS)
dev.off()



blues <- sequential_hcl(3, h = 230, c = c(10, 50), l = c(80, 10), power=1)
pal(blues)

############### Conditional Frequent Set Analysis for index medications #########


rownames(data) <- data[["champid"]]
############ Eclat analysis for most frequent patterns overall ########
data %>% filter(ANYGS==1) %>% select(-champid, -ANYGS) -> data_for_eclat_df

newEC_Control <- new("ECcontrol", verbose = FALSE)

list_of_itemsets <- sapply(colnames(data_for_eclat_df), 
                           function(x){
                           calculate_rule_qual_measures(
                               idx_colname = x,
                               data    = data_for_eclat_df, 
                               support = 0.1, 
                               maxlen  = 10,
                               control = newEC_Control
                               )
                           }, simplify=FALSE, USE.NAMES=TRUE
                           )

pdf(file=paste0(".\\Output\\", format(Sys.time(), "%Y_%m_%d"), "_Heatmaps_Interesting_Sets_Per_Medication_with_GS.pdf"), width=10, height=7, paper="special")


for (i in names(list_of_itemsets)){
tmp_data <- list_of_itemsets[[i]]    
interestingness_idx <- quality(tmp_data)$Lift > 2 & is.closed(tmp_data) & 
                        quality(tmp_data)$Confidence > 0.2 & 
                        quality(tmp_data)$N_AC > 4  # at least four cases in the indexed data set

interesting_sets <- subset(tmp_data, interestingness_idx)
binary_df_long <- melt(as(items(x=interesting_sets), "matrix"), na.rm=TRUE)

binary_df_long <- mutate(binary_df_long, 
                         Lift   = value * quality(interesting_sets)$Lift, 
                         NCases = value * quality(interesting_sets)$transIdenticalToItemsets)
#print(paste(max(binary_df_long$Lift), max(binary_df_long$NCases), sep=" "))

#binary_df_long <- melt(binary_df_long, id.vars=c("Var1", "Var2"))
binary_df_long$Var1 <- as.factor(binary_df_long$Var1)
#print(nlevels(binary_df_long$Var1))
    
    if (nrow(binary_df_long) > 1 ){ 
        plot(assemble_setplot(
                         data               = binary_df_long, 
                         x                  = "Var2", 
                         y                  = "Var1", 
                         color              = "Lift", 
                         size               = "NCases",
                         legend_title_size  = "#Cases",
                         legend_title_color = "Lift",
                         title_text         = paste("Medications frequently used with", i), 
                         maxcolor           = 15, 
                         max_size           = 55, 
                         n_y                = nlevels(binary_df_long$Var1)
                        ))
            } else {
                print(paste0("No interesting sets found for ", i)) 
                plot(-1,-1, xlim=c(0,1), ylim=c(0,1), axes=FALSE, frame.plot=FALSE, xlab="", ylab="", 
                text(0.5,0.5, paste0("No interesting sets found for ", i)) , col="white")
                }  
}
dev.off() 


############ Eclat analysis for most frequent patterns for cases without Geriatric Syndromes ########
data %>% filter(ANYGS==0) %>% select(-champid, -ANYGS) -> data_for_eclat_df

newEC_Control <- new("ECcontrol", verbose = FALSE)

list_of_itemsets <- sapply(colnames(data_for_eclat_df), 
                           function(x){
                           calculate_rule_qual_measures(
                               idx_colname = x,
                               data    = data_for_eclat_df, 
                               support = 0.1, 
                               maxlen  = 10,
                               control = newEC_Control
                               )
                           }, simplify=FALSE, USE.NAMES=TRUE
                           )

inspect(subset(list_of_itemsets$antiplat, Lift > 2  & Confidence > 0.2 & N_AC > 4))

pdf(file=paste0(".\\Output\\", format(Sys.time(), "%Y_%m_%d"), "_Heatmaps_Interesting_Sets_Per_Medication_without_GS.pdf"), width=10, height=7, paper="special")


for (i in names(list_of_itemsets)){
tmp_data <- list_of_itemsets[[i]]    
interestingness_idx <- quality(tmp_data)$Lift > 2 & is.closed(tmp_data) & 
                        quality(tmp_data)$Confidence > 0.2 & 
                        quality(tmp_data)$N_AC > 4  # at least four cases in the indexed data set

interesting_sets <- subset(tmp_data, interestingness_idx)
binary_df_long <- melt(as(items(x=interesting_sets), "matrix"), na.rm=TRUE)

binary_df_long <- mutate(binary_df_long, 
                         Lift   = value * quality(interesting_sets)$Lift, 
                         NCases = value * quality(interesting_sets)$transIdenticalToItemsets)
#print(paste(max(binary_df_long$Lift), max(binary_df_long$NCases), sep=" "))

#binary_df_long <- melt(binary_df_long, id.vars=c("Var1", "Var2"))
binary_df_long$Var1 <- as.factor(binary_df_long$Var1)
#print(nlevels(binary_df_long$Var1))
    
    if (nrow(binary_df_long) > 1 ){ 
        plot(assemble_setplot(
                         data               = binary_df_long, 
                         x                  = "Var2", 
                         y                  = "Var1", 
                         color              = "Lift", 
                         size               = "NCases",
                         legend_title_size  = "#Cases",
                         legend_title_color = "Lift",
                         title_text         = paste("Medications frequently used with", i), 
                         maxcolor           = 15, 
                         max_size           = 55, 
                         n_y                = nlevels(binary_df_long$Var1)
                        ))
            } else {
                print(paste0("No interesting sets found for ", i)) 
                plot(-1,-1, xlim=c(0,1), ylim=c(0,1), axes=FALSE, frame.plot=FALSE, xlab="", ylab="", 
                text(0.5,0.5, paste0("No interesting sets found for ", i)) , col="white")
                }  
}
dev.off() 






############### Conditional Frequent Set Analysis for index diseases in columns two, three and four #########

#select data and run eclatz analysis on the full data set and the subset conditional on the index disease 
newEC_Control <- new("ECcontrol", verbose = FALSE)
blues <- sequential_hcl(3, h = 230, c = c(10, 50), l = c(80, 10), power=1)
pal(blues)
index_diseases <- names(data)[2]

list_of_itemsets_dis <- list()
for (disease in index_diseases){

tmp_data <- cbind(select_(data, disease), select(data, antigout:antidiab))

analysis <- calculate_rule_qual_measures(
                                           idx_colname = disease,
                                           data    = tmp_data, 
                                           support = 0.05, 
                                           maxlen  = 10,
                                           control = newEC_Control
                                           )

list_of_itemsets_dis[[disease]] <- analysis
}
### this returns another named list, here run in a for loop


inspect(head(sort(list_of_itemsets_dis$ANYGS, by="Confidence", decreasing=FALSE)))

inspect(subset(list_of_itemsets_dis$ANYGS, N_AC > 10  & Confidence > 0.01 & Lift > 2))
### the named list goes into another for loop - this one creates a plot for each list elements (or says that there are no interesting sets
#to be found. Interestingness is again defined under the conditions in the interestingness_idx
pdf(file=paste0(".\\Output\\", format(Sys.time(), "%Y_%m_%d"), "_Heatmaps_Medication_And_Conditions.pdf"), width=10, height=7, paper="special")

for (i in names(list_of_itemsets_dis)){
tmp_data <- list_of_itemsets_dis[[i]]    
interestingness_idx <- quality(tmp_data)$Lift > 2 & is.closed(tmp_data) & 
                        quality(tmp_data)$Confidence > 0.01 & 
                        quality(tmp_data)$N_AC > 10  # at least four cases in the indexed data set

interesting_sets <- subset(tmp_data, interestingness_idx)
binary_df_long <- melt(as(items(x=interesting_sets), "matrix"), na.rm=TRUE)

binary_df_long <- mutate(binary_df_long, 
                         Lift   = value * quality(interesting_sets)$Lift, 
                         NCases = value * quality(interesting_sets)$transIdenticalToItemsets)
#print(paste(max(binary_df_long$Lift), max(binary_df_long$NCases), sep=" "))

#binary_df_long <- melt(binary_df_long, id.vars=c("Var1", "Var2"))
binary_df_long$Var1 <- as.factor(binary_df_long$Var1)
#print(nlevels(binary_df_long$Var1))
    
    if (nrow(binary_df_long) > 1 ){ 
        plot(assemble_setplot(
                         data               = binary_df_long, 
                         x                  = "Var2", 
                         y                  = "Var1", 
                         color              = "Lift", 
                         size               = "NCases",
                         legend_title_size  = "#Cases",
                         legend_title_color = "Lift", 
                         title_text         = paste("Medications frequently used with", i), 
                         maxcolor           = 3, 
                         max_size           = 20, 
                         n_y                = nlevels(binary_df_long$Var1)
                        ))
            } else {
                print(paste0("No interesting sets found for ", i)) 
                plot(-1,-1, xlim=c(0,1), ylim=c(0,1), axes=FALSE, frame.plot=FALSE, xlab="", ylab="", 
                text(0.5,0.5, paste0("No interesting sets found for ", i)) , col="white")
                }  
}
dev.off() 





















